#bottle REST API
from bottle import Bottle, run, request, response, get, post, delete
from bottle import static_file
import json
import searchalgorithm
import os

app = Bottle()

#global variables
pathToPdf = 'C:\Voor elkaar\Code\david\pdfs'

#this method gets 1 document based on its name
@app.get('/documents/<name>')
def fetchDoc(name):
    fullPath = pathToPdf + '\\' + name
    
    if(os.path.isfile(fullPath)):
        response.status = '200 OK'
        return static_file(name, root = pathToPdf)

    response.status = '404 Not Found'
    return json.dumps({'error': 'NotFoundError'})


@app.get('/documents/')
def searchDocumentBase():
    parentInfo = {}
    childInfo = {}
    try:
        #get the searchquery from the request
        question = request.query['question']

    except KeyError:
        response.status = '400 Bad Request'
        response.content_type = 'application/json'
        return json.dumps({'error' : 'QuestionError'})

    if 'pRegion' in request.GET:
        #get the parentInfo
        parentInfo['pRegion'] = request.query['pRegion']

    #get the childInfo if it is there
    #when there is a child age (cAge) then there is childInfo
    if 'cAge' in request.GET:
        #print('childinfo is set')
        childInfo['cAge'] = request.query['cAge']
        childInfo['cEduSituation'] = request.query['cEduSituation']
        childInfo['cEduType'] = request.query['cEduType']
        #value of cHasLimitation is actually not important since we won't send it along if the child
        #has no limitation
        childInfo['cHasLimitation'] = request.query['cHasLimitation']
        if childInfo['cHasLimitation'] == '1':
            childInfo['cLimitationCat'] = request.query['cLimitationCat']
            childInfo['cLimitationDesc'] = request.query['cLimitationDesc']    
   
    docList = searchalgorithm.search(question, parentInfo, childInfo)
    resultList = []
       
    #remove the full paths
    for doc in docList:
        filename = doc.split('\\')[-1]
        filename = filename[:-4]
        resultList.append(filename)
        
    response.status = '200 OK'
    resp = {'result' : resultList}
    return json.dumps(resp)


@app.post('/documents/')
def saveNewDoc(url):
    #use the url to call the scraper
    #check if the scraper gives the document or saves it
    #REST API should save it for uniformity sake
    return ''


#should change the directory from dev environment to server environment
@app.delete('/documents/<name>')
def deleteDoc(name):
    pathToPdf = 'C:\Voor elkaar\Code\david\pdfs\\' + name

    #need to remove the .pdf extension and add the .txt extension
    name, ext = os.path.splitext(name)
    pathToTxt = 'C:\Voor elkaar\Code\david\docs\\' + name + '.txt'

    try:
        os.remove(pathToTxt)
        os.remove(pathToPdf)

        response.status = '200 Ok'
        return 

    except OSError:
        response.status = '404 Not Found'
        return json.dumps({'error': 'This document can not be found'})
    
    
run(app, host='localhost', port='8080')
