#David Klein
#last updated: 09-11-2016
#search algorithm

import string
import math
import os
import re
from collections import Counter

#
#
#   BM25 Start
#
#

def splitString(keywordString):
    #make a set of the characters to include, in this case punctuation
    #then take these out of the string by joining all characters not in exclude
    exclude = set(string.punctuation)
    keywordString = ''.join(ch for ch in keywordString if ch not in exclude)
    return keywordString.split()


#this method checks whether a certain keyword is in a document
#if it is we need to save in which document so we later can check these documents for frequency
#
#document is the string for the file, so for example 'amputatie.txt'
def checkIfInDocument(document, keyword):    
    with open(document, encoding = 'utf-8') as f:               #this gives only readaccess since 'r' is omitted
        for line in f:
            if keyword.lower() in line.lower():                 #ignore casing
                return True
    return False            

def checkAllDocsForKeyword(keyword):
    totalDocs = 0
    directory = 'C:\\Voor elkaar\\Code\\david\\docs\\'
    for filename in os.listdir(directory):
        fullPath = os.path.join(directory, filename)
        if checkIfInDocument(fullPath, keyword):
            totalDocs += 1
    return totalDocs

#count the total number of files in directory but dont read them into memory
#can hardcode the directory for now but these directories will change if tagbased system will be
#implemented with different directories
def numberOfFiles():
    directory = 'C:\\Voor elkaar\\Code\\david\\docs\\'
    num_files = sum(os.path.isfile(os.path.join(directory, f)) for f in os.listdir(directory))
    return num_files
    
            

#this method calculates the inverse document frequency based on the formula    
def IDF(keyword):
    totalDocNr = numberOfFiles()
    DF = checkAllDocsForKeyword(keyword) / totalDocNr
    numer = (totalDocNr - DF) + 0.5
    denom = DF + 0.5

    #IDF shouldnt be allowed to be negative, because that will have a negative impact on the total score
    #IDF will be negative if more than half of the corpus' documents has the keyword, since then the denom
    #will be larger then the numer and the log of a number which is 0 < number < 1 is always negative
    if numer <= 0:
        numer = 0

    if denom <= 0:
        denom = 1
        
    frac = numer / denom
    return math.log10(frac)


#document is full path 
def getDocLength(document):
    c = Counter()
    with open(document, encoding = 'utf-8') as f:
            for line in f:
                c.update(line.split())

    return sum(c.values())
    
#calculate the average document length in the corpus by using the Counter class
#splits every line in the file in words then adds those to c
#counting of words is hard to check since notepad++, word and python give 3 different answers
#as long as we use python to count we have the same deviation for every corpus
def averageDocLength():
    totalDocLength = 0
    directory = 'C:\\Voor elkaar\\Code\\david\\docs\\'
    for filename in os.listdir(directory):
        fullPath = os.path.join(directory, filename)
        totalDocLength += getDocLength(fullPath)
        
    return math.ceil(totalDocLength/numberOfFiles())


#calculates the termfrequency for a given document and keyword
def getTF(document, keyword):
    docLength = getDocLength(document)
    count = 0     

    with open(document, encoding = 'utf-8') as f:
        for line in f:
            words = line.split()
            for word in words:
                if keyword.lower() in word.lower():
                    count += 1

    return count/docLength


def docScorePerKeyword(document, keyword):   
    k = 2.0                         #okapi bm25 standard
    b = 0.75                        #okapi bm25 standard
    
    docLength = getDocLength(document)
    avgDocLength = averageDocLength()
    
    
    numer = getTF(document, keyword)*(k + 1)
    denom = getTF(document, keyword)+(k*(1-b+b*(docLength/avgDocLength)))

    return IDF(keyword) * (numer/denom)

#this method gives a dictionary of all files with their scores for the question    
def scoresForQuestion(question, parentInfo, childInfo):
    directory = 'C:\\Voor elkaar\\Code\\david\\docs\\'
    scoreDict = dict()

    keywords = splitString(question)                          #split the question into keywords

    #if there is parentInfo then it is added to the keywords here
    if parentInfo:
        for value in parentInfo:
            keywords.append(value)

    #if there is childInfo then it is added to the keywords here
    if childInfo:
        for val in childInfo:
            valwords = splitString(val)
            for word in valwords:
                keywords.append(word)

    for filename in os.listdir(directory):                      #loop through the entire directory
        fullPath = os.path.join(directory, filename)

        docQuestionScore = 0
                
        for word in keywords:                                   #check the score per keyword and add that to total
            docQuestionScore += docScorePerKeyword(fullPath, word)
            

        scoreDict[fullPath] = docQuestionScore
        
    return scoreDict


#
#
#   BM25 End
#
#


#
#
#   ParentProfile Start
#
#

#keywords are needed to add to the total of keywords that BM25 uses
def getParentKeywords(parentDict):
    keywords = []
    if 'pRegion' in parentDict:
        keywords.append(parentDict['pRegion'])
    
    return keywords

#
#
#   ParentProfile End
#
#

#
#
#   ChildProfile Start
#
#
def getChildKeywords(childDict):
    keywords = []
    #how is age to be searched for? Maybe regex?
    if 'cAge' in childDict:
        keywords.append(childDict['cAge'])
        
    if 'cEduSituation' in childDict:
        keywords.append(childDict['cEduSituation'])
        
    if 'cEduType' in childDict:
        keywords.append(childDict['cEduType'])
        
    if 'cHasLimitation' in childDict:
        if 'cLimitationCat' in childDict:
            
            keywords.append(childDict['cLimitationCat'])
            
        if 'cLimitationDesc' in childDict:
            keywords.append(childDict['cLimitationDesc'])

    return keywords
        


#
#
#   ChildProfile End
#
#

#
#
#   General code Start
#
#

#this gets called from 
def search(question, parentDict, childDict):
    parentInfo = getParentKeywords(parentDict)
    childInfo = getChildKeywords(childDict)

    return sortDictOnValue(scoresForQuestion(question, parentInfo, childInfo))


#reverse=True makes it so that it sorts from high to low
def sortDictOnValue(scoreDict):
    sortedList = sorted(scoreDict, key=scoreDict.__getitem__, reverse=True)
    return sortedList

#
#
#   General code End
#
#

#print(checkAllDocsForKeyword('sport'))

#print(splitQuestion("Wat is amputatie?"))
#print(getDocLength('C:\\HBO Zuyd\\Studiejaar 3\\BM01\\Casus\\data\\amputatie.txt'))
#print(getTF('C:\\HBO Zuyd\\Studiejaar 3\\BM01\\Casus\\data\\amputatie.txt', 'amputatie'))
#print(numberOfFiles())
#print(averageDocLength())
#print(checkAllDocsForKeyword('amputatie'))
#print(docScorePerKeyword('C:\\Voor elkaar\\Code\\david\\docs\\amputatie.txt', 'amputatie'))
#print(docScorePerKeyword('C:\\Voor elkaar\\Code\\david\\docs\\downsyndroom.txt', 'amputatie'))

#try with no parentInfo
#print(scoresForQuestion('Wat is fietsen?', []))
#print()
#try with parentInfo
#print(scoresForQuestion('Wat is fietsen?', ['Heuvelland'], []))
#print()
#return sorted list
#print(search('Wat is fietsen?', {'pRegion': 'Heuvelland'}, {'cAge': '7', 'cEduSituation' : 'basisschool', 'cEduType' : 'speciaal onderwijs', 'cHasLimitation' : 1, 'cLimitationCat' : 'chronisch', 'cLimitationDesc' : 'been amputatie'}))

